INSTALL PACKAGE:
npm install

TECHNOLOGY:

- NodeJS
- GraphQL

BUILD:
npm build

RUN WITH:

- DEVELOPMENT: npm start:dev
  required environment: npm install -g typescript
- PRODUCTION: npm start

ROUTE LINK API: http://localhost:3000/graphql
