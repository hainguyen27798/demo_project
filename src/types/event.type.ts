import { GraphQLObjectType, GraphQLNonNull, GraphQLID, GraphQLString, GraphQLFloat } from 'graphql';
import { EventObject } from '../inputs/event.input';

export default new GraphQLObjectType({
	name: 'Events',
	fields: () => ({
		_id: { type: new GraphQLNonNull(GraphQLID) },
		...EventObject
	})
});
