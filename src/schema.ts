import { GraphQLSchema, GraphQLObjectType } from 'graphql';

import rootQuery from './queries';
import rootMutation from './mutation';

export default new GraphQLSchema({
	query: new GraphQLObjectType({
		name: 'rootQuery',
		fields: rootQuery
	}),
	mutation: new GraphQLObjectType({
		name: 'rootMutation',
		fields: rootMutation
	})
});
