import mongoose from './database.connection';

const Schema = mongoose.Schema;

const EventSchema = new Schema({
	title: { type: String, required: true },
	discription: { type: String, required: true },
	price: { type: Number, required: true },
	date: { type: Number, required: true }
});

export default mongoose.model('Event', EventSchema);
