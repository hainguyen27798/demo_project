import mongoose from 'mongoose';

mongoose.connect(`mongodb://localhost:27017/${process.env.MONGO_DB}`, { useNewUrlParser: true, useUnifiedTopology: true }, err => {
	if (err) {
		console.log(err);
	}
});

mongoose.connection;

export default mongoose;
