import { GraphQLInputObjectType, GraphQLString, GraphQLFloat } from 'graphql';

export default new GraphQLInputObjectType({
	name: 'EventInput',
	fields: () => EventObject
});

export const EventObject = {
	title: { type: GraphQLString },
	discription: { type: GraphQLString },
	price: { type: GraphQLFloat },
	date: { type: GraphQLString }
};
