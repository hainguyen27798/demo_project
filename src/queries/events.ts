import { GraphQLList } from 'graphql';
import eventType from '../types/event.type';
import { EventModel } from '../models';

export default {
	type: new GraphQLList(eventType),
	resolve() {
		return EventModel.find().then(events => events.map(e => ({ ...e['_doc'], date: new Date(parseInt(e['_doc']['date'])).toString() })));
	}
};
