import eventType from '../types/event.type';
import eventInput from '../inputs/event.input';
import { EventModel } from '../models';

export default {
	type: eventType,
	args: {
		input: { type: eventInput }
	},
	resolve(_, { input }: any) {
		input.date = new Date(input.date);
		const eventModel = new EventModel(input);
		return eventModel.save();
	}
};
