import express from 'express';
import bodyParser from 'body-parser';
import graphqlHTTP from 'express-graphql';
import schema from './schema';

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.get('/', (req, res, next) => {
	res.send('Hello Server!');
});

app.use(
	'/graphql',
	graphqlHTTP({
		schema,
		graphiql: true
	})
);

app.listen(port, () => {
	console.log(`listening port ${port}...`);
});
